<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller

{
    public function index()
     /*------------controller for index page-------*/
    {
        return view('frontend.index');
    }
    /*------------controller for home page-------*/
    public function home()

    {
        return view('frontend.home');
    }
    /*------------controller for about us page-------*/
    public function aboutus()

    {
        $title="SEE OUR SUCCESSFUL PROJECTS";
        $scope=['#','NAME OF PROJECTS','DATE OF LAUNCH'];
        $data=[];
        $data[]=["THE BREAD FACTORY","2002"];
        $data[]=["THE SKINNERS SCHOOL","2003"];
        $data[]=["SWINGERS","2006"];
        $data[]=["ANYVAN","2008"];
        $data[]=["SPECSAVERS","2010"];

        return view('frontend.aboutus',compact('scope','title','data'));
    }
    /*------------controller for contact us page-------*/

    public function contactus()

    {
        return view('frontend.contactus');
    }
    /*------------controller for privacyandpolicy page-------*/
    public function privacyandpolicy()

    {
        return view('frontend.privacyandpolicy');
    }
    /*------------controller for termsandconditions page-------*/
    public function termsandconditions()

    {
        return view('frontend.termsandconditions');
    }


}