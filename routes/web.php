<?php

use App\Http\Controllers\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*------------route for index page-------*/
Route::get('/', [FrontendController::class,'index'])->name('frontend.index');
/*------------route for about us page-------*/
Route::get('/aboutus',[FrontendController::class,'aboutus'])->name('frontend.aboutus');
/*------------route for contact us page-------*/
Route::get('/contactus',[FrontendController::class,'contactus'])->name('frontend.contactus');
/*------------route for home page-------*/
Route::get('/home',[FrontendController::class,'home'])->name('frontend.home');
/*------------route for privacyandpolicy page-------*/
Route::get('/privacyandpolicy',[FrontendController::class,'privacyandpolicy'])->name('frontend.privacyandpolicy');
/*------------route for termsandconditions page-------*/
Route::get('/termsandconditions',[FrontendController::class,'termsandconditions'])->name('frontend.termsandconditions');