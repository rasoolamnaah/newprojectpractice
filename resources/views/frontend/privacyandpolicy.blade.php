@extends('layouts.frontend')
@include('layouts.inc.navbar')
@section('title')
    PRIVACY AND POLICY
 @endsection


 @section('content')

 <div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
             <h1> <i>PRIVACY AND POLICY </i> </h1>
             <p><i>All information received by us from your registration on business-standard.com or other digital products of Business Standard will be used by Business Standard in accordance with our Privacy Policy.
                 <br><b> Kindly read the below mentioned details.</b>
                <br>On registration, we expect you to provide Business Standard with an accurate and complete information of the compulsory fields. We also expect you to keep the information secure, specifically access passwords and payment information. Kindly update the information periodically to keep your account relevant. Business Standard will rely on any information you provide to us.</i>
                 <br>
                 
                </p>
        </div>
       </div>
</div>

@include('frontend.slider')



@endsection



 @section('scripts')


 @endsection

