@extends('layouts.frontend')
@include('layouts.inc.navbar')
@section('title')
    About us
 @endsection


 @section('content')

 <div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
             <h1> About Us </h1>
             <p><i>Since 2001, Infinity Group have provided our clients with the complete business solution which includes IT Support, Digital Transformation, network Connectivity and IT Consultancy Services. We take care of everything for you, helping to keep your business running efficiently using the latest technology.<br>
                As one of the largest IT Support and IT Consultancy providers, with offices in six locations across the UK in London, Kent, Surrey, Birmingham and East and West Sussex, we have helped thousands of clients providing IT Support Services, Digital Transformation and Microsoft Dynamics 365 Consultancy to organisations across the UK and Ireland<br>
                We are proud to be Microsoft Gold partners and are also accredited partners with several other leading industry providers including Cisco Meraki, Mitel, Cisco, Sophos, Ruckus, WatchGuard and more. Infinity Group also hold several highly regarded certifications including ISO 27001 for Information Security Management Standard, ISO 9001 and 14001.<br>
            </i></p>
        </div>
       </div>
</div>

<table class="table table-bordered table-info">
    
        <h1> {{$title}}  </h1>
    <thead>
      <tr>
          @foreach($scope as $sc)
              
          
        <th scope="col">{{$sc}}</th>
        @endforeach
        
      </tr>
    </thead>
    <tbody>
     @foreach($data as $word)
        
      <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$word[0]}}</td>
        <td>{{$word[1]}}</td>
        
        
      </tr>
      @endforeach
    </tbody>
  </table>



 @endsection



 @section('scripts')


 @endsection



