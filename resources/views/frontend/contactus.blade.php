@extends('layouts.frontend')
@include('layouts.inc.navbar')
@section('title')
    CONTACT US
 @endsection


 @section('content')

 <div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
             <h1> CONTACT US  </h1>
             <p><i>Please call us on the number above or use the live chat feature at the bottom of your screen.<br>
                 Alternatively, if you prefer, you can complete our contact form below and a Consultant will be in touch.</i>
                 <br>
                 <b>Sales:03303 333 648</b><br>
                 <b>Support:0345 450 4600</b><br>
                </p>
        </div>
       </div>
</div>

 @include('frontend.slider')



 @endsection



 @section('scripts')


 @endsection

