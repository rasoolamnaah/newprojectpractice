
<div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('frontend/slider/4.jpg')}}"  class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>MEET OUR TEAM</h5>
          <p><b>Let's work together, starting today</b></p>
          <div class="slider-btn">
            <button class="btn btn-1">Get IN Touch</button>
            <button class="btn btn-2">See Demo</button>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('frontend/slider/6.jpg')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>How much will your app cost?</h5>
          <p><b>Send us the features you are looking to build and we shall advise on the next steps.</b></p>
          <div class="slider-btn">
            <button class="btn btn-2">Get Free Quote</button>
          </div>
        </div>
      </div>
      
      
      <div class="carousel-item">
        <img src="{{ asset('frontend/slider/2.jpg')}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>What are our clients saying?</h5>
          <p><b>We empower our clients to bring about real change, change to drive innovation and maximise business growth.</b> </p>
          <div class="slider-btn">
            <button class="btn btn-2">See Testimonials</button>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('frontend/slider/5.png')}}"  class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Development to the Core</h5>
          <p><b>We build affordable teachnology for Organizations tackling the toughest problems facing the people and the planet.</b></p>
          <div class="slider-btn">
            
            <button class="btn btn-2">Get Started</button>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<!-------create-------->
<section id="create">
  <div class="description-container">
    <img src="frontend/slider/8.jpg" alt="" class="img-responsive floating-image right">

    <div class="row">
      <div class="col-md-6">
        <h2>Building a Better World With Technology </h2>
        <div class="create-paragraph">
          <p>As the leading bespoke software development company in the United Kingdom, we have the expertise to
         achieve results others can’t. We provide a complete frontend and backend development service that can be
          tailored to any industry.<br>
          Our proficiencies span many continents, sectors and challenges. Whatever complex problem you're trying to
           solve, we'll help you find a solution, together.</p>
        </div>
        <button type="button" class="btn btn-success">Read More>></button>
        <div class="col-md-6">
        </div>
      </div>
      <!------------Cards with Hover effects------->
      <section id="card">
      <div class="header">
        <div class="row">
          <h1>OUR MOST SUCCESSFUL PROJECT AND ITS RATING</h1>
        <div class="col-md-4">
          
          <div class="card shadow" style="width: 20rem;">
            <div class="inner">
              
              <img src="frontend/slider/13.jpg" class="card-img-top" alt="...">
            </div>
            
            <div class="card-body text-center">
              <h2 class="card-title">3BLUE DOTS</h2>
              <p class="card-text">Blue Dots Technology is a talent and workforce management company established in 2009.<br>
                Its core business is to assist employers and recruitment sectors to stay ahead of their competition by
                 increasing efficiency, reducing costs, raising productivity, adding value and protecting margins.<br>
                  3 Blue Dots Technologies is a UK based company that offers recruitment and HR automation software together with support and consultancy services.
                </p>
 
              <a href="#" class="btn btn-success">See More</a>
            </div>
          </div>
        </div>
        <div class="col-md-6 Success-rate">
          <p>Customer Satisfaction </p>
          <div class="progress">
          <div class="progress-bar" style="width: 80%;">
            80%
          </div>
          </div>
          <p>Customer Interaction with Us </p>
          <div class="progress">
          <div class="progress-bar" style="width: 80%;">
            80%
          </div>
          </div>
          <p>Employee Work Satisfaction </p>
          <div class="progress">
          <div class="progress-bar" style="width: 80%;">
            80%
          </div>
          </div>
          <p>Company Satisfaction </p>
          <div class="progress">
          <div class="progress-bar" style="width: 80%;">
            80%
          </div>
          </div>
         
         
        </div>       
      </div> 
       </div> 
      </section>

